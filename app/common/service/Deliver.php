<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/1
 * Time: 11:43
 */

namespace app\common\service;


class Deliver
{
    public static function send_by_email($email,$title,$content){
       return Email::send(
            $email,
            $title,
            Email::content($title,$content)
        );
    }
}