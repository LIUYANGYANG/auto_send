{__NOLAYOUT__}<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>跳转提示</title>
 
    <style type="text/css">
    	*{
    		padding:0;
    		margin:0;
    	}
        #error_tips{
        	width: 420px;
        	margin: 211px auto;
        }
        .success_img{
        	width: 280px;
        	height: 166px;     
        	margin: 0 auto;  	
        }
        .success_wz{
        	
        	text-align: center;
        	font-size:20px  ;
        	color: #333333;
        	margin: 30px 0px;
        	min-width: 420px;
        	text-align: center;
        }
        .success_wz p{
            min-width: 420px;  	
            text-align: center;
        }
        #ShowDiv{
        	margin: 20px 0px;
        	min-width:420px;
        }
        .error_cont{
        	text-align: center;
        }
        .error_cont a{
        	margin: 0 auto;
        	display: block;
        	width: 180px;
        	height: 40px;
        	border: 2px solid #84B142;
        	text-decoration:none;
        	color: #84b142;
        	font-size: 20px;
        	line-height: 40px;
        	border-radius: 20px ;
        }
    </style>
</head>
<body onload="Load('<?php echo($url);?>')">
    <div class="wrap">
	  <div id="error_tips">
	  	<div class="success_img">
	  		<img src="__PUBLIC__/image/chenggong.png" alt="" />
	  	</div>
	  	<div class="success_wz">
	  		<p><?php echo(strip_tags($msg));?></p>
	  		<div id='ShowDiv'>
	        	
	        </div>
	  	</div>
	    <div class="error_cont">
	      <a href="<?php echo($url);?>" class="btn">返回</a>
	    </div>
	  </div>
	</div>
</body>
</html>
	<script>
		var secs = 3; //倒计时的秒数 
		var URL;
		function Load(url) {
			URL = url;
			for(var i = secs; i >= 0; i--) {
				window.setTimeout('doUpdate(' + i + ')', (secs - i) * 1000);
			}
		}
		function doUpdate(num) {
			document.getElementById('ShowDiv').innerHTML = '将在' + num + '秒后自动跳转页面';
			if(num == 0) {
				location.href = '<?php echo($url);?>';
			}
		}
	</script>