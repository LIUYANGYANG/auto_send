<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/1
 * Time: 10:51
 */

namespace app\common;


class Alipay
{
    public static $one_require = false;

    public static function load()
    {
        if (!self::$one_require) {
            require __DIR__ . "/vendor/Alipay/Corefunction.php";
            require __DIR__ . "/vendor/Alipay/Md5function.php";
            require __DIR__ . "/vendor/Alipay/Notify.php";
            require __DIR__ . "/vendor/Alipay/Submit.php";
            self::$one_require = true;
        }
    }
    public static function do_alipay($code, $fee)
    {
        $alipay_config = array(
            'partner' => config('alipay_partner'),   //这里是你在成功申请支付宝接口后获取到的PID；
            'key' => config('alipay_key'),//这里是你在成功申请支付宝接口后获取到的Key
            'private_key_path' => 'key/rsa_private_key.pem',
            'ali_public_key_path' => 'key/alipay_public_key.pem',
            'sign_type' => strtoupper('MD5'),
            'input_charset' => 'utf-8',
            'cacert' => getcwd() . '\\cacert.pem',
            'transport' => 'http',
        );
        /**
         * 支付成功跳转路径，同步通知地址
         */
        $return_url = sprintf("%s/%s/code/%s",
            config('site_url'),
            "index/order/alipay_notify",
            $code); ; //页面跳转同步通知页面路径
        $notify_url = sprintf("%s/%s/code/%s",
            config('site_url'),
            "index/order/alipay_notify_back",
            $code); //服务器异步通知页面路径

        $payment_type = "1"; //支付类型 //必填，不能修改
        $seller_email = config('alipay_seller_email');//卖家支付宝帐户必填
        $out_trade_no = $code;//商户订单号 通过支付页面的表单进行传递，注意要唯一！
        $subject = "对" . $code . '交易号付款';  //订单名称 //必填 通过支付页面的表单进行传递
        $total_fee = $fee;   //付款金额  //必填 通过支付页面的表单进行传递
        $body = '订单付款';
        $anti_phishing_key = "";//防钓鱼时间戳 //若要使用请调用类文件submit中的query_timestamp函数
        $user_IP = isset($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
        $user_IP = isset($user_IP) ? $user_IP : $_SERVER["REMOTE_ADDR"];
        $exter_invoke_ip = $user_IP; //客户端的IP地址
        /************************************************************/
        $extra_common_param = '1010101';//test
        //构造要请求的参数数组，无需改动
        $parameter = array(
            "service" => "create_direct_pay_by_user",
            "partner" => trim(config('alipay_partner')),
            "payment_type" => $payment_type,
            "notify_url" => $notify_url,
            "return_url" => $return_url,
            "seller_email" => $seller_email,
            "out_trade_no" => $out_trade_no,
            "subject" => $subject,
            "total_fee" => $total_fee,
            "body" => $body,
            "extra_common_param" => $extra_common_param,
            "anti_phishing_key" => $anti_phishing_key,
            "exter_invoke_ip" => $exter_invoke_ip,
            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
        );
        //建立请求
        /** @var  \AlipaySubmit $alipaySubmit */
        $alipaySubmit = new \AlipaySubmit($alipay_config);
        $html_text = $alipaySubmit->buildRequestForm($parameter, "post", "确认");
        echo $html_text;
        return false;
    }
}