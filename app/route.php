<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

\think\Route::get('admin/nav', function(){
	require APP_PATH."/admin/nav.json";
});

/**
 * 验证码地址
 */
\think\Route::get('captcha/[:id]', "\\think\\captcha\\CaptchaController@index");
 

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    // '__miss__'  => '/static/404.html',
];

