<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/1
 * Time: 10:48
 */

namespace app\index\controller;


use app\common\Alipay;

class Notify extends Common
{
    /**
     * @param int $code
     * @return string
     */
    function alipay_notify_back($code = 0)
    {
        Alipay::load();
        //这里我们通过TP的C函数把配置项参数读出，赋给$alipay_config；
        $alipay_config = array(
            'partner' => config('alipay_partner'),   //这里是你在成功申请支付宝接口后获取到的PID；
            'key' => config('alipay_key'),//这里是你在成功申请支付宝接口后获取到的Key
            'private_key_path' => 'key/rsa_private_key.pem',
            'ali_public_key_path' => 'key/alipay_public_key.pem',
            'sign_type' => strtoupper('MD5'),
            'input_charset' => 'utf-8',
            'cacert' => getcwd() . '\\cacert.pem',
            'transport' => 'http',
        );
        //计算得出通知验证结果
        $get_temp = $_GET;
        unset($_GET['uid']);
        unset($_GET['payid']);
        unset($_GET['type']);

        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        //恢复$_GET数组
        $_GET = $get_temp;
        unset($get_temp);
        if ($verify_result) {
            if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                echo $code;
                return ("支付成功");
            }
        } else {
            return ("支付失败");
        }
        return false;
    }

    /**
     * @param int $code
     */
    function alipay_notify($code = 0)
    {
        Alipay::load();
        //这里我们通过TP的C函数把配置项参数读出，赋给$alipay_config；
        $alipay_config = array(
            'partner' => config('alipay_partner'),   //这里是你在成功申请支付宝接口后获取到的PID；
            'key' => config('alipay_key'),//这里是你在成功申请支付宝接口后获取到的Key
            'private_key_path' => 'key/rsa_private_key.pem',
            'ali_public_key_path' => 'key/alipay_public_key.pem',
            'sign_type' => strtoupper('MD5'),
            'input_charset' => 'utf-8',
            'cacert' => getcwd() . '\\cacert.pem',
            'transport' => 'http',
        );

        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();

        if ($verify_result) {
            if ($_REQUEST['trade_status'] == 'TRADE_SUCCESS') {
                echo $code;
                exit("支付成功");
            }
            exit("支付失败");
        } else {
            //验证失败

            exit("验证失败");
//            $this->error("支付失败", url('/myorder'));
        }
    }


}