<?php
namespace app\admin\controller;


use app\common\Auth;
use app\common\controller\Admin;
use app\common\model\Users;
use think\Hook;

class Index extends Admin
{
    public function index()
    {
        Hook::listen('auth_begin');
        return view();
    }

    /**
     * 登陆
     * @param int $name
     * @param int $pwd
     * @return array|mixed|\think\response\View
     */
    public function login($name = 0, $pwd = 0)
    {
        if ($name && $pwd) {
            $user = Users::getUser(function ($query) use ($name, $pwd) {
                return $query->where([
                    "email" => $name,
                    "password" => $pwd,
                ]);
            });
            do {
                if (empty($user)) {
                    $this->error("密码错误");
                } else {
                    Auth::login($user);
                    $this->success("登陆成功", '/admin');
                }
            } while (0);
        }
        return view();
    }

    /**
     * 登出
     */
    public function logout()
    {
        session("user_id", 0);
        $this->success("登出成功", '/admin/index/login');
    }

    /**
     * 测试
     */
    public function test()
    {
        $user = new Users();
        $user->email = rand(0, 19999);
        $user->password = 111;
        $user->surname = 111;
        $user->givenname = 111;
        $user->wechat = 111;
        $user->facebook = 111;
        $user->telephonevis = 111;
        $user->telephonevis = 111;
        $user->save();

        dump(Users::all());
    }
}
