<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/10/28 0028
 * Time: 上午 10:20
 */

namespace app\admin\controller;


use app\common\controller\Admin;

class Exposition extends Admin
{
    public function banner(){
        return view();
    }
    public function edit_banner(){
        return view();
    }
    public function edit_expos(){
        return view();
    }
    public function edit_expos_detail(){
        return view();
    }
    public function official(){
        return view();
    }
    public function personal(){
        return view();
    }
    public function school(){
        return view();
    }
    public function recommend(){
        return view();
    }
    public function other(){
        return view();
    }
    public function show_image($src=''){
        if(empty($src)){
            $this->error("image not exist!!!");
        }
        $this->assign('src',$src);
        return view();
    }
}