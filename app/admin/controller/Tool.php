<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/10/28 0028
 * Time: 上午 10:13
 */

namespace app\admin\controller;


use app\common\controller\Admin;
use app\common\Upload;

class Tool extends Admin
{
    /**
     * 文件上传接口
     * 上传注意  input name=file type=file
     * 返回 :
     * code  //0是正常
     * msg
     * data[src] //图片地址
     */
    public function upload()
    {
        if (request()->file('file')) {
            $file_path = Upload::save_file();
            if ($file_path) {
                return json([
                    "code" => 0,
                    "msg" => "success",
                    "data" => [
                        'src'=>$file_path
                    ],
                ]);
            } else {
                return json([
                    "code" => -1,
                    "msg" => "error",
                    "data" => "error",
                ]);
            }

        }
        return json([
            "code" => -1,
            "msg" => "error",
            "data" => "error",
        ]);
    }
}