<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:49:"E:\code\auto_send/app/admin\view\index\index.html";i:1509332623;s:51:"E:\code\auto_send/app/admin\view\public\header.html";i:1509345480;s:51:"E:\code\auto_send/app/admin\view\public\footer.html";i:1509332623;}*/ ?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>manage</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="__PUBLIC__/V1/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="__PUBLIC__/V1/css/global.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/plugins/font-awesome/css/font-awesome.min.css">

    <script src="__PUBLIC__/V1/js/jquery-1.9.0.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="__PUBLIC__/V1/js/Chart.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css">
        .fl{
            float: left;
            margin: 20px;
        }
        .fl p{
            width: 200px;text-align: center;
        }
        .fl p:last-of-type{
            width: 280px;
        }
        .fl:last-of-type{
            margin-bottom: 100px;
        }
    </style>
</head>

<body>
<div class="layui-layout layui-layout-admin" style="border-bottom: solid 5px #1aa094;">
    
    <div class="layui-header header header-demo">
        <div class="layui-main">
            <div class="admin-login-box">
                <a class="logo" style="left: 0;" href="/admin">
                    <span style="font-size: 22px;">OuiLine</span>
                </a>
                <div class="admin-side-toggle">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <div class="admin-side-full">
                    <i class="fa fa-life-bouy" aria-hidden="true"></i>
                </div>
            </div>
            <ul class="layui-nav admin-header-item">
                <li class="layui-nav-item">
                    <a href="">clear cache</a>
                </li>
                <li class="layui-nav-item">
                    <a href="/" target="_blank">website</a>
                </li>

                <li class="layui-nav-item">
                    <a href="javascript:;" class="admin-header-user">
                        <img src="__PUBLIC__/V1/images/0.jpg" />
                        <span>manager|</span>
                        <span>tie</span>
                    </a>
                    <dl class="layui-nav-child">
                        <!--<dd>-->
                            <!--<a href="javascript:;"><i class="fa fa-user-circle" aria-hidden="true"></i> userinfo</a>-->
                        <!--</dd>-->
                        <!--<dd>
                            <a href="javascript:;"><i class="fa fa-gear" aria-hidden="true"></i> 设置</a>
                        </dd>
                        <dd id="lock">
                            <a href="javascript:;">
                                <i class="fa fa-lock" aria-hidden="true" style="padding-right: 3px;padding-left: 1px;"></i> 锁屏 (Alt+L)
                            </a>
                        </dd>-->
                        <dd>
                            <a href="<?php echo url('admin/index/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> logout</a>
                        </dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav admin-header-item-mobile">
                <li class="layui-nav-item">
                    <a href="login.html"><i class="fa fa-sign-out" aria-hidden="true"></i> logout</a>
                </li>
            </ul>
        </div>
    </div>
    <!---sidebar--->
    <div class="layui-side layui-bg-black" id="admin-side">
        <div class="layui-side-scroll" id="admin-navbar-side" lay-filter="side"></div>
    </div>

    <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
        <div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
            <ul class="layui-tab-title">
                <li class="layui-this">
                    <i class="fa fa-dashboard" aria-hidden="true"></i>
                    <cite>panel</cite>
                </li>
            </ul>
            <div class="layui-tab-content" style="min-height: 150px; padding: 5px 0 0 0;">
                <div class="layui-tab-item layui-show">
                   

                </div>
            </div>
        </div>
    </div>

    <div class="layui-footer footer footer-demo" id="admin-footer">
    <div class="layui-main">
        <p>2017 &copy;
            <a href="/">ouiline</a> copy right
        </p>
    </div>
</div>
<div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
</div>
<div class="site-mobile-shade"></div>


<script type="text/javascript" src="__PUBLIC__/V1/plugins/layui/layui.js"></script>
<script>

    /** index.js By Beginner Emain:zheng_jinfan@126.com HomePage:http://www.zhengjinfan.cn */

    var tab;

    layui.config({
        base: '/static/V1/js/',
        version:new Date().getTime()
    }).use(['element', 'layer', 'navbar', 'tab'], function() {
        var element = layui.element(),
                $ = layui.jquery,
                layer = layui.layer,
                navbar = layui.navbar();
        tab = layui.tab({
            elem: '.admin-nav-card' //设置选项卡容器
            ,
            //maxSetting: {
            //	max: 5,
            //	tipMsg: '只能开5个哇，不能再开了。真的。'
            //},
            contextMenu:true
        });
        //iframe自适应
        $(window).on('resize', function() {
            var $content = $('.admin-nav-card .layui-tab-content');
            $content.height($(this).height() - 147);
            $content.find('iframe').each(function() {
                $(this).height($content.height());
            });
        }).resize();

        //设置navbar
        navbar.set({
            spreadOne: true,
            elem: '#admin-navbar-side',
            cached: false,
            //data: navs,
            url: '/admin/nav'
            //url: '/admin/tool/sidebar_menu'
        });
        //渲染navbar
        navbar.render();
        //监听点击事件
        navbar.on('click(side)', function(data) {
            tab.tabAdd(data.field);
        });

        $('.admin-side-toggle').on('click', function() {
            var sideWidth = $('#admin-side').width();
            if(sideWidth === 200) {
                $('#admin-body').animate({
                    left: '0'
                }); //admin-footer
                $('#admin-footer').animate({
                    left: '0'
                });
                $('#admin-side').animate({
                    width: '0'
                });
            } else {
                $('#admin-body').animate({
                    left: '200px'
                });
                $('#admin-footer').animate({
                    left: '200px'
                });
                $('#admin-side').animate({
                    width: '200px'
                });
            }
        });
        $('.admin-side-full').on('click', function() {
            var docElm = document.documentElement;
            //W3C
            if(docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            //FireFox
            else if(docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            //Chrome等
            else if(docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
            //IE11
            else if(elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
            layer.msg('按Esc即可退出全屏');
        });




        //手机设备的简单适配
        var treeMobile = $('.site-tree-mobile'),
                shadeMobile = $('.site-mobile-shade');
        treeMobile.on('click', function() {
            $('body').addClass('site-mobile');
        });
        shadeMobile.on('click', function() {
            $('body').removeClass('site-mobile');
        });
    });


</script>

</div>
</body>

</html> 