<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:66:"E:\code\auto_send\www/../app/admin\view\exposition\edit_expos.html";i:1509436560;s:58:"E:\code\auto_send\www/../app/admin\view\public\common.html";i:1509410901;}*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="cn">
<head>
    <title>ouiline</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" href="__PUBLIC__/V1/css/global.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/css/main.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/css/table.css" media="all">
	<link rel="stylesheet" href="__CSS__/back.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/plugins/layui/css/layui.css" media="all">
    <script type="text/javascript" src="__JS__/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="__PUBLIC__/layer/layer.js"></script>
	<script src="__PUBLIC__/V1/plugins/layui/layui.js" charset="utf-8"></script>
	<script type="text/javascript" src="__JS__/common.js"></script>

</head> 
<body class="frame-body">
<script>
	function model(url,callback){
		layer.open({
				type: 2,
				title: false,
				offset: '20px',
				area: ['840px', '460px'],
				shadeClose: true,
				closeBtn: 1,
				scrollbar: true,
				cancel: function () {
					if(callback){
						callback();
					}else{
						location.reload();
					}
				},
				content: [
					url
					]
        });
	}
	layui.use(['form','element','upload'], function () {
		upload = layui.upload;
	});
</script>


<a href="javascript:history.back()" class="layui-btn layui-btn-small">返回</a>


<div class="layui-tab">
    <ul class="layui-tab-title">
        <li class="layui-this">编辑详情</li>
        <li>编辑单品</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend> 展览 </legend>
            </fieldset>

            <form class="layui-form" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">title</label>

                    <div class="layui-input-block">
                        <input type="text" name="title" autocomplete="off" placeholder=""
                               class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">type</label>

                    <div class="layui-input-block">
                        <input type="text" name="type" autocomplete="off" placeholder=""
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">starttime</label>

                    <div class="layui-input-block">
                        <input type="text" name="starttime" onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">endtime</label>

                    <div class="layui-input-block">
                        <input type="text" name="endtime" onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" autocomplete="off" class="layui-input">
                    </div>
                </div> 

                <div class="layui-form-item">
                    <label class="layui-form-label">cost</label>

                    <div class="layui-input-block">
                        <input type="text" name="cost" autocomplete="off" placeholder=""
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">taglist</label>

                    <div class="layui-input-block">
                        <input type="text" name="taglist" autocomplete="off" placeholder=""
                               class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">subject</label>

                    <div class="layui-input-block">
                        <input type="text" name="subject" autocomplete="off" placeholder=""
                               class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">abstract</label>

                    <div class="layui-input-block">
                        <textarea class="layui-textarea layui-hide" name="abstract" id="content">    </textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn"  name="submit" value="0">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>

            </form>
        </div>
        <div class="layui-tab-item">

            <a href="javascript:model('/admin/exposition/edit_expos_detail')" class="layui-btn layui-btn-small">新增</a>

            <table class="layui-table">
                <thead>
                <tr>
                    <th >scasc</th>
                    <th >scasc</th>
                    <th >scasc</th>
                    <th >scasc</th>
                    <th >scasc</th>
                </tr>
                </thead>
                <tbody>
                <tr class="">
                    <td >0.00</td>
                    <td >123323244</td>
                    <td >scasc </td>
                    <td >2017-10-19 10:34:19</td>
                    <td >
                        <a href="javascript:model('/admin/exposition/edit_expos_detail')" class="layui-btn layui-btn-small">编辑</a>
                    </td>
                </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>

<script>
    layui.use(['form', 'layedit', 'laydate','element'], function () {
        var form = layui.form()
                , layer = layui.layer
                , layedit = layui.layedit
                ,  element = layui.element
                , laydate = layui.laydate;
        layedit.set({
            uploadImage: {
                url: '/admin/tool/upload', //接口url
                type: 'post'//默认post
            }
        });
        //构建一个默认的编辑器
        index = layedit.build('content');
    });

</script>