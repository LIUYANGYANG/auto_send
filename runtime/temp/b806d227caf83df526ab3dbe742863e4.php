<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:56:"E:\code\auto_send\www/../app/admin\view\index\login.html";i:1509352093;s:58:"E:\code\auto_send\www/../app/admin\view\public\common.html";i:1509410901;}*/ ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="cn">
<head>
    <title>ouiline</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" href="__PUBLIC__/V1/css/global.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/css/main.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/css/table.css" media="all">
	<link rel="stylesheet" href="__CSS__/back.css" media="all">
    <link rel="stylesheet" href="__PUBLIC__/V1/plugins/layui/css/layui.css" media="all">
    <script type="text/javascript" src="__JS__/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="__PUBLIC__/layer/layer.js"></script>
	<script src="__PUBLIC__/V1/plugins/layui/layui.js" charset="utf-8"></script>
	<script type="text/javascript" src="__JS__/common.js"></script>

</head> 
<body class="frame-body">
<script>
	function model(url,callback){
		layer.open({
				type: 2,
				title: false,
				offset: '20px',
				area: ['840px', '460px'],
				shadeClose: true,
				closeBtn: 1,
				scrollbar: true,
				cancel: function () {
					if(callback){
						callback();
					}else{
						location.reload();
					}
				},
				content: [
					url
					]
        });
	}
	layui.use(['form','element','upload'], function () {
		upload = layui.upload;
	});
</script>
 
<script type='text/javascript' src='__JS__/particleground.js'></script>

	<!--<div class="login-background"></div>-->
	<div id="login_box">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>登录</legend>
    </fieldset>

    <form class="layui-form" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-block">
                <input type="text" name="name" lay-verify="title" autocomplete="off" placeholder="请输入用户名" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-block">
                <input type="password" name="pwd" lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
    </div>
	<script>
	 //粒子背景特效
	  $('body').particleground({
		dotColor: '#ccc',
		density: 10000,
		lineColor: '#ddd'
	  }); 
	</script>